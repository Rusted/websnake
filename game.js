//constants
var cw =  14
var snakelength = 1;

// all globals here or stuff that needs debug info
var foodArr = []
var whiteArr = [] 
var obstacleArr = []
var snakeArr = [] 
var w,h
var d
var game_loop
var cycle
var directionChanged
var main_canvas_element, levelImageElement
var canvasContext

$(document).ready(function(){
	
	//Makes all screen canvas  without scrolling
	$('body').css('margin', 0 )
	//$(main_canvas_element).css({left:0,top:0})
	levelImageElement = document.getElementById('myimg')
	main_canvas_element  = document.getElementById('canvas')
	canvasContext = main_canvas_element.getContext("2d")
	//setting canvas size
	initCanvasSize()
	w = main_canvas_element.width
	h = main_canvas_element.height
		
//=================================================== START==============================================================================================================================================================
	init();
	
	function init()
	{
		cycle=0;
		d = "right";
		foodArr.length=0; 
		score = 0;

		//draws level image on white rect 
		canvasContext.fillStyle = "white"
		canvasContext.fillRect(0, 0, w, h)
		canvasContext.drawImage(levelImageElement, 0, 0);

		create_snake();
		create_food(); 

		if (typeof game_loop != "undefined"){
			clearInterval(game_loop);
		}
		game_loop = setInterval(paint, 50);

		initWhiteAndObstacleCells();
	}


	
//========================================================== LEVEL PAINT LOGIC =================================================================================================================
	function paint(){

		cycle++;
		//allows to change direction
		directionChanged = false;
		//clears the level every frame
		canvasContext.fillStyle = "white";
		canvasContext.fillRect(0, 0, w, h);
	
		
		//draws level image on white rect 
		canvasContext.drawImage(levelImageElement, 0, 0);
		
		//snake head position
		var nx = snakeArr[0].x;
		var ny = snakeArr[0].y;
		//creating a cell object with current head coordinates
		
		//where snake wants to go based on direction
		if(d == "right") nx++;
		else if(d == "left") nx--;
		else if(d == "up") ny--;
		else if(d == "down") ny++;
		
		//draws white cells for every cell inside array
		empty_cells(whiteArr);

//====================== COLLISION CHECK LOGIC =========================================
		
		//teleportable walls	
		if (nx >= w/cw){	
				nx = 0;		
		}	
		if (nx <= -1){	
				nx = w/cw-1;		
		}
		if (ny>= h/cw){	
				ny = 0;		
		}	
		if (ny <= -1){		
				ny = h/cw-1;		
		}

		//game restarts if snake collision or black cell collision
		if  (check_collision(nx, ny, snakeArr))  //|| cellContainsColor(nx, ny, 0)) // ||(check_maze (nx,ny,w,cw,h)))
		{
			//old flag not to restart game if collision with blue pixel which is food
			if (!(foodCollectedLastFrame)){
				init();
				return;
			}
		}
		//used to set this flag to avoid game restart collecting food pixel
		var foodCollectedLastFrame = false;
		

		//if snake head is on food
		var cellToCheck = {x:nx, y:ny};

		if(removeCell(foodArr,cellToCheck))
		{
			var cellArrThing = {x: nx, y: ny}
		//adding head position to empty cell array if not exists
		if (checkCell(whiteArr,cellArrThing)){
			whiteArr.push(cellArrThing);
		}
			//making new head cell object (blue pixel, because z= false)
			var newHead = {x: nx, y: ny, z:canvasContext.getImageData(nx*cw,ny*cw,cw,cw)};
			score++;
			//insert food head to beginning of array
			snakeArr.unshift(newHead);
			foodCollectedLastFrame = true;
			//Create new food
					
		}

		if (foodArr.length==0){
				create_food();
		}
		//snake eating map logic
		/*
		else if (check_maze(nx,ny,w,cw,h)){
			//cutting out piece of map, z = cell with picture data
			var newHead = {x: nx, y: ny, z:canvasContext.getImageData(nx*cw,ny*cw,cw,cw)};
			//adding new head with image
			snakeArr.unshift(newHead);
			score++;		
		}
		*/
//============================== MOVEMENT LOGIC =========================================

		//if nothing was in a way of snake, it moves				
		else
		{
			//moving the snake coordinates starting from last element			
			for(var j=snakeArr.length-1;j>0;j--){
				snakeArr[j].x = snakeArr[j-1].x;
				snakeArr[j].y = snakeArr[j-1].y;
			}
			//new head direction becomes first element
			snakeArr[0].x = nx;
			snakeArr[0].y = ny;
		}

//============================== FINAL PAINTINGS AFTER LOGIC ==============================================	

		//Food was either changed if picked up or remained the same, need to repaint the food either way
		paint_cells(foodArr);

		//painting snake cells based on coordinates
		for(var i = 0; i < snakeArr.length; i++)
		{
			var c = snakeArr[i];
			paint_cell(c.x, c.y, c.z, "BLUE");
		}
		canvasContext.strokeStyle = "RED";
		canvasContext.strokeRect(0, 0, w, h);

//============================== MISCALLENIOUS ==========================================================	
		
		//Dicks
		var score_text = "Score: " + score;
		canvasContext.fillText(score_text, 5, h-5);
	}
	
	
	//Lets add the keyboard controls now
	$(document).keydown(function(e){
		var key = e.which;
		if (!(directionChanged)){
		//We will add another clause to prevent reverse gear
			if((key=="65" || key == "37") && d != "right" && d != "left"){
			 	d = "left";
				directionChanged = true;
			}
			else if((key== "87" || key == "38") && d != "down" && d != "up"){
			 	d = "up";
				directionChanged = true;
			}
			else if((key== "68" || key == "39") && d != "left" && d != "right"){
			 	d = "right";
				directionChanged = true;
			}
			else if((key== "83" ||key == "40") && d != "up" && d != "down"){
				d = "down";
				directionChanged = true;
			}
		}
		
		
		//else if(key == "40" ) window.clearInterval(paint);
		//The snake is now keyboard controllable
	})
	
})
