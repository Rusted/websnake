/**
  @license html2canvas v0.33 <http://html2canvas.hertzen.com>
  Copyright (c) 2011 Niklas von Hertzen. All rights reserved.
  http://www.twitter.com/niklasvh

  Released under MIT License
 */


 
(function( $ ){
    $.fn.html2canvas = function(options) {
		
        if (options && options.profile && window.console && window.console.profile) {
            console.profile();
        }
        var date = new Date(),
        $message = null,
        timeoutTimer = false,
        timer = date.getTime();
        options = options || {};
        options.elements = this;
	options.proxy=false;
        //options.flashcanvas = "../external/flashcanvas.min.js";
       
        html2canvas.logging = options && options.logging;
        options.complete = function(images){
            var queue = html2canvas.Parse(this[0], images, options),
            $canvas = $(html2canvas.Renderer(queue, options)),
            finishTime = new Date();

            if (options && options.profile && window.console && window.console.profileEnd) {
                console.profileEnd();
            }
            $canvas.css({
                position: 'absolute', 
                left: 0, 
                top: 0
            }).appendTo(document.body);
    	//$canvas.css({width: '100px', height: '100px', display:'none'})
	var c= $("canvas")[0]
	var ctx=c.getContext("2d");
	var img=$("canvas")[0]

	//ctx.drawImage(img,window.scrollY,window.scrollX, screen.availHeight, screen.availWidth);
/*
	$("canvas").attr("width", screen.availWidth);    
	$("canvas").attr("height", screen.availHeight);
*/
	Canvas2Image.saveAsPNG($("canvas")[0]);
	    //shows canvas NOT RLY, some faggotism
   	$canvas.toggle();

	    try {
		$canvas[0].toDataURL();
	    } catch(e) {
		if ($canvas[0].nodeName.toLowerCase() === "canvas") {
		    alert("Canvas is tainted, unable to read data");
		}
	    }
            
        };
        html2canvas.Preload(this[0],  options);


    };
})( jQuery );


