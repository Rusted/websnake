# Description

This is a snake mini-game chrome plugin. This plugin converts current webpage into a snake level.
Preview here: https://www.youtube.com/watch?v=mdpkPwlBxkY

# Installation
- Create a .crx archive with all the files from the repository
- Use this guide to install chrome plugins in dev mode: https://stackoverflow.com/a/24577660

# Overview

this plugin converts a current webpage html into canvas and then converts it into a game level.
