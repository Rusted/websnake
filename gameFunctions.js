//========================================================== MY FAVORITE INIT FUNCTIONS =================================================

function initCanvasSize(){

	main_canvas_element.width = 600
	main_canvas_element.height  = 600
	//setting canvas size for cell width
	while ((main_canvas_element.width % cw) != 0){
		main_canvas_element.width--
	}

	while ((main_canvas_element.height % cw) != 0){
		main_canvas_element.height--
	}
}

function initWhiteAndObstacleCells(){

	for (var i=0;i<w/cw;i++){
		for (var j=0;j<h/cw;j++){
			var cell = {x:i, y:j};
			if (isCellDark(cell)){
				if (!arrayContainsCell(obstacleArr,cell))
					obstacleArr.push(cell)
			}
			if (isCellBright(cell)){
				if (!arrayContainsCell(whiteArr,cell))
					whiteArr.push(cell)
			}
		}
	}
}

//========================================================== snake init ======================================================================
function create_snake()
{
	snakeArr = []; 
	for(var i = snakelength-1; i>=0; i--)
	{	
		snakeArr.push({x: i, y:0, z:false});
	}
}
//==================================================== FOOD FUNCTIONS ==================================================================================
//!!!!!!!!!!!!!!!!!!!TODO ADD TIMER FALLBACK TO NOT LAGGY FUNCTION	
	//creates food based on snake length
	function create_food()
	{
		var foodie = randFirstFoodCell()
		var justArr = []
		justArr.unshift(foodie)
		//var start = new Date().getTime();
		foodArr = generateRecursiveFood(justArr, Math.floor(Math.random()*snakeArr.length))
		//var end = new Date().getTime();
		//var time = end - start

	}

	//generates random food cell on unoccupied slot
	function randFirstFoodCell(){
		var foodx
		var foody
		var ac
		//random cell coordinates
		foodx = Math.round(Math.random()*(w-cw)/cw)
		foody = Math.round(Math.random()*(h-cw)/cw)
		ac = averageColor (foodx,foody);
		c = {x:foodx, y:foody}
		//while random cell is black, generate random coordinates
		while (whiteOrObstacle(c)){
			
			foodx = Math.round(Math.random()*(w-cw)/cw)
			foody = Math.round(Math.random()*(h-cw)/cw)
			c = {x:foodx, y:foody}
			ac = averageColor (foodx,foody);
		}
		mainFood = {
			x: foodx, 
			y: foody, 
			z: canvasContext.getImageData(foodx*cw,foody*cw,cw,cw),
			ac: ac
		}
		return mainFood
	}
	
	function generateRecursiveFood(arr,n){
		if (n<=0){
			return arr
		} 
		else{
			avgAC = arrayAverageColor(arr)
			c = getMostSimilarCell(arr[0], avgAC, arr)
			//console.log("similar",c);
			if (c==false){
				return arr
			}
			arr.unshift(c)
			return generateRecursiveFood(arr, n-1)
		}	
	}
//================================================================ COLOR DIFFERENCE FUNCTIONS ===================

	function whiteOrObstacle(c){
		if ((arrayContainsCell (whiteArr,c)) || (arrayContainsCell (obstacleArr,c))){
			return true;
		}
		return false;
	}

	function isCellBright(cell){
		var avgac = averageColor (cell.x,cell.y);
		if ((avgac.r+avgac.g+avgac.b) >=700){

			return true;
		}

		return false;
	}

	function isCellDark(cell){
		var avgac = averageColor (cell.x,cell.y);
		if ((avgac.r+avgac.g+avgac.b) <=100){

			return true;
		}

		return false;
	}
	
	function arrayAverageColor(arr){
		var totalac = {r:0,g:0,b:0}
		var avgac = {r:0,g:0,b:0}
		for (var i=0;i<arr.length;i++){
			totalac.r = totalac.r + arr[i].ac.r
			totalac.g = totalac.g + arr[i].ac.g
			totalac.b = totalac.b + arr[i].ac.b
		}
		avgac.r = totalac.r / arr.length
		avgac.g = totalac.g / arr.length
		avgac.b = totalac.b / arr.length
		return avgac
	}
	

	function averageColor (nx,ny){
		var myData = canvasContext.getImageData( nx*cw, ny*cw, cw, cw);
		var r=0;
		var g=0;
		var b=0;
		for (var i=0;i<myData.data.length;i+=4)
	    	{
			r += myData.data[i];
			g += myData.data[i+1];
			b += myData.data[i+2];
		}
		cellPixels = (cw*cw);
		r = r/cellPixels;
		g = g/cellPixels;
		b = b/cellPixels;
		var returnColor = {r: r,g: g,b: b};
		return returnColor;	
	}


	function ColourDistance(e1, e2)
	{
	    rmean = (e1.r + e2.r ) / 2;
	    r = e1.r - e2.r;
	    g = e1.g - e2.g;
	    b = e1.b - e2.b;
	    return Math.sqrt((((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8));
	}

	function getMostSimilarCell(c, avgAC, arr){
		
		var	potentialArr = [];
		for (var i=0;i<arr.length;i++){
			c = arr[i]
			var	foodXL = left(c.x,w)
			var	foodXR = right(c.x,w)
			var	foodYL = left(c.y,h)
			var	foodYR = right(c.y,h)
			
			c1 = {x:foodXL, y:c.y, ac:0, cd:0 }
			c2 = {x:foodXR, y:c.y, ac:0, cd:0 }
			c3 = {x:c.x, y:foodYL, ac:0, cd:0 }
			c4 = {x:c.x, y:foodYR, ac:0, cd:0 }

			if ( !arrayContainsCell(arr,c1) && !arrayContainsCell(potentialArr,c1)){
				if (!whiteOrObstacle(c1)){
					c1 = {x:foodXL, y:c.y, ac:averageColor(foodXL,c.y), cd: ColourDistance(averageColor(foodXL,c.y), avgAC) }
					potentialArr.push(c1);		
				}			
			}	
			if ( !arrayContainsCell(arr,c2) && !arrayContainsCell(potentialArr,c2)){
				if (!whiteOrObstacle(c2)){
					c2 = {x:foodXR, y:c.y, ac:averageColor(foodXR,c.y), cd: ColourDistance(averageColor(foodXR,c.y), avgAC) }
					potentialArr.push(c2);		
				}
				
			}
			if ( !arrayContainsCell(arr,c3) && !arrayContainsCell(potentialArr,c3)){
				if (!whiteOrObstacle(c3)){
					c3 = {x:c.x, y:foodYL, ac:averageColor(c.x,foodYL), cd: ColourDistance(averageColor(c.x,foodYL), avgAC) }
					potentialArr.push(c3);		
				}
			}
			if ( !arrayContainsCell(arr,c4) && !arrayContainsCell(potentialArr,c4)){
				if (!whiteOrObstacle(c4)){
					c4 = {x:c.x, y:foodYR, ac:averageColor(c.x,foodYR), cd: ColourDistance(averageColor(c.x,foodYR), avgAC) }
					potentialArr.push(c4);		
				}
			}
		}
		if (potentialArr.length==0){
			return false;		
		}
		var lowestcd = potentialArr[0].cd;
		var lowestArr = [];
		for (var i=0;i<potentialArr.length;i++){
			if (potentialArr[i].cd<lowestcd){
				lowestcd = potentialArr[i].cd;		
			}
		}

		for (var i=0;i<potentialArr.length;i++){
			if (potentialArr[i].cd == lowestcd){
				lowestArr.push(potentialArr[i]);		
			}
		}		
		var desiredIndex = Math.floor(Math.random() * lowestArr.length);
		var cellInArr = lowestArr.splice(desiredIndex, 1);
		return cellInArr[0];	
	}

//==================================================================== CELL ARRAY FUNCTIONS ================================	
	//checks array of cells if it contains cell with coordinates, used for snake cell collision
	function check_collision(x, y, array)
	{
		for(var i = 0; i < array.length; i++)
		{
			if(array[i].x == x && array[i].y == y)
			 return true;
		}
		return false;
	}

	//checking if empty cell array contains cell 
	function checkCell(arr,cell){
		for (var i=0;i<arr.length;i++){
			if ( (arr[i].x==cell.x ) && (arr[i].y==cell.y) ){
				return  false
			}
		}
		return true;
	}

	function arrayContainsCell(arr,cell){
		for (var i=0;i<arr.length;i++){
			if ( (arr[i].x==cell.x ) && (arr[i].y==cell.y) ){
				return  true
			}
		}
		return false;
	}

	function removeCell(arr,cell){
		for (var i=0;i<arr.length;i++){
			if ( (arr[i].x==cell.x ) && (arr[i].y==cell.y) ){
				arr.splice(i,1);
				return  true;
			}
		}
		return false;
	}

//=========================================================== COORDINATE FUNCTIONS==================================================================

function right(x,wh){

	if (x>= wh/cw-1){
			return 0;
		} else{
			return x+1;
		}
	}

function left(x,wh){

	if (x<=0){
		return wh/cw-1;
	} else{
		return x-1;
	}
}	
//================================================================ CELL FUNCTIONS ===============================================================
	function paint_cellsZ(arr){
		for(var i = 0; i < arr.length; i++){
		
			if (cycle%2 ==0){
				paint_cell(arr[i].x,arr[i].y, arr[i].z,"GREEN");
			}
			else{
				paint_cell(arr[i].x,arr[i].y, arr[i].z,"RED");
			}
		}
	}

	function paint_cells(arr){
		for(var i = 0; i < arr.length; i++){
			paint_cell(arr[i].x,arr[i].y, false,"GREEN");
		}
	}

	//Paints a cell based on coordinates and last parameter
	function paint_cell(x, y, z,color)
	{
		//if z=false, paints a snake/generic food square
		if (z==false){
			color = "RED";
			if (cycle%60 ==0){
				color = "YELLOW"			
			}
			//canvasContext.fillStyle = "blue";
			//canvasContext.fillRect(x*cw, y*cw, cw, cw);
			canvasContext.strokeStyle = color;
			
			canvasContext.strokeRect(x*cw, y*cw, cw, cw);
		
		}
		//if z is not false, it's an imageData object, paint it with a border
		else{
			canvasContext.putImageData(z,x*cw, y*cw);	
			canvasContext.strokeStyle = color;
			canvasContext.strokeRect(x*cw, y*cw, cw, cw);
		}	
	}
	
	//calls empty cell function for every cell in array
	function empty_cells(cellArr){
		for(var i = 0; i < cellArr.length; i++){
			empty_cell(cellArr[i].x,cellArr[i].y);
		}	
	}
	
	//draws white square for given cell
	function empty_cell(x,y){
		canvasContext.fillStyle = "white";
		canvasContext.fillRect(x*cw, y*cw, cw, cw);
		//canvasContext.strokeStyle = "fdf5e6";
		//canvasContext.strokeRect(x*cw, y*cw, cw, cw);
	}
